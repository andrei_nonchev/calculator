<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ChrisKonnertz\StringCalc\StringCalc;

class DefaultController extends AbstractController
{
    public function index()
    {
        $content=$this->render('calculator.html.twig')->getContent();	
        $response=new Response;
        $response->setContent($content);
        $response->headers->set('Cache-Control','no-cache');

        return $response;
    }
	

    public function ajax()
    {
	    $request=$this->container->get('request_stack')->getCurrentRequest();
		
		$data=array('result'=>'','error'=>'');
		
		$stringCalc = new StringCalc();
		$expression = trim($request->get('expression'));
		$expression=str_replace("÷","/",$expression); //This is the correct division character

		try{
			$result = $stringCalc->calculate($expression);
			if($result)
				$data['result']=$result;
			else
				$data['error']="Sorry, we couldn't calculate the expression.";
		}catch(\Exception $e){
			$data['error']="Sorry, we couldn't calculate the expression.";
		}
		
	    $response=new Response;
	    $response->headers->set('Content-Type', 'application/json');
	    
	    $response->setContent(json_encode($data));
	    return $response;
    }
	
    public function test()
    {
		$tests=array(
			array('expression'=>'2+2','expectation'=>'4'),
			array('expression'=>'5--2','expectation'=>'7'),
			array('expression'=>'10/2','expectation'=>'5'),
			array('expression'=>'50*5','expectation'=>'250'),
			array('expression'=>'1+2+3+4','expectation'=>'10'),
			array('expression'=>'-1','expectation'=>'-1'),
			array('expression'=>'2+2','expectation'=>'5') // just to show one wrong expectation
		);
		
		$stringCalc = new StringCalc();
		
		foreach($tests as $k=>$t){
			$result = $stringCalc->calculate($t['expression']);
			$tests[$k]['result']=$result;
			if($result!=$t['expectation'])
				$tests[$k]['color']='red';
			else
				$tests[$k]['color']='green';
		}
		
		$content=$this->render('test.html.twig',array('tests'=>$tests))->getContent();	
        $response=new Response;
        $response->setContent($content);
        $response->headers->set('Cache-Control','no-cache');
	
		return $response;
    }
	
}