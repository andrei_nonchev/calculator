$(function(){
	
	$('.td').on('click',function(){
		var newChar=$(this).html();
	
		//Clears the current expression
		if(newChar=='C'){
			$('#input').val('');
			$('#error').html('').hide();
			return;
		}
		
		var expr=$('#input').val();
		if(validateInput(expr,newChar)){
			$('#input').val(expr+newChar);
			$('#error').html('').hide();
			return;
		}
		
		$('#error').html('Invalid expression.').show();
	});
	
	
	$('#submit').on('click',function(){
		
		var params={
			'expression':$('#input').val(),
		};
		
		$.post(ajax_url,params, function(data){
			if(data.error!=''){
				$('#error').html(data.error).show();
			}else{
				$('#input').val(data.result);
				$('#error').html('').hide();
			}
			
		}).fail(function(){
			$('#error').html("Sorry, our calculator service is down at the moment.").show();
		});
	});
	
	$('#test').on('click',function(){
		runTests();
	});	
});

//Checks if the addition of the latest symbol to the current expression is allowed
function validateInput(expr,newChar){
	
	var newCharType=getCharType(newChar);
		
	if(expr.length==0){
		if(newCharType=='digit' || newChar=='-')return true; //You can only start an expression with a digit or minus sign for a negative number
		return false;
	}
	
	var lastChar = expr.charAt(expr.length-1);
	var lastCharType=getCharType(lastChar);
	
	//You can always add digits to the expression
	if(newCharType=='digit')
		return true; 
	
	//We always expect a digit after a decimal separator
	if(lastCharType=='dot' && newCharType!='digit')
		return false;

	//You can only add a decimal separator after a digit
	if(newCharType=='dot' && lastCharType!='digit')
		return false;
	
	//We parse the expression backwards to check for a number with invalid double decimal separtors eg. 123.456.
	if(newCharType=='dot'){
		for(var i=expr.length-1;i>=0;i--){
			var charType=getCharType(expr[i]);
			if(charType=='sign')
				break;
			if(charType=='dot')
				return false;
		}
		return true;
	}
	
	//These signs can only be added after a digit since they always signify an operation
	if((newChar=='+' || newChar=='÷' || newChar=='*') && lastCharType!='digit')
		return false;
	
	//The minus sign is an exception since it can also signify a negative number 
	if(newChar=='-'){
		
		if(expr.length>1){
			var penultimateChar=expr.charAt(expr.length-2);
			console.log(lastChar+''+penultimateChar);
			
			//You can only add two minus signs in a row in the case when the first signifies an operation and the second signifies a negative number;
			if(lastChar=='-' && penultimateChar=='-')
				return false;
			
			return true;
		}else{
			//You cannot start an expression with two minus signs
			if(lastChar=='-')
				return false;
		}
	}
	
	return true;
}

function getCharType(c){
	
	if(c=='÷' || c=='*' || c=='+' || c=='-')
		return 'sign';

	if(c=='.')
		return 'dot';
	
	if($.isNumeric(c))
		return 'digit';
	
	return null;
}


function runTests(){
	
	var tests=[
	 {'expression':'123+','newChar':'*','expectation':false},
	 {'expression':'123.55','newChar':'.','expectation':false},
	 {'expression':'123+10--','newChar':'-','expectation':false},
	 {'expression':'1-','newChar':'-','expectation':true},
	 {'expression':'1--','newChar':'-','expectation':false},
	 {'expression':'2*','newChar':'2','expectation':true},
	 {'expression':'123+','newChar':'5','expectation':true},
	 {'expression':'1','newChar':'2','expectation':false} //just to show one wrong expectation output
	];

	var str='<table class="test_table"><tr><th>Expression</th><th>New Character</th><th>Expеctation</th><th>Result</th></tr>';

	for(var i=0;i<tests.length;i++){
		
		var result=validateInput(tests[i].expression,tests[i].newChar);

		if(result!=tests[i].expectation)
			var color='red';
		else 
			var color='green';
		
		str+='<tr style="color:'+color+';"><td>'+tests[i].expression+'</td><td>'+tests[i].newChar+'</td><td>'+tests[i].expectation+'</td><td>'+result+'</td></tr>';
	}
	str+='</table>';
	
	$('#test_output').html(str);
}